#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){
    int base;
    unsigned exponente;
    int resultado=1;

    printf ("Introduce la BASE: ");
    scanf ("%i",&base);

    printf ("Introduce un EXPONENTE: ");
    scanf ("%u",&exponente);

    for (unsigned mul=0; mul<exponente; mul++)
        resultado *= base;
    printf("RESULTADO: %i^%u = %i\n", base, exponente, resultado);

    return EXIT_SUCCESS;
}
