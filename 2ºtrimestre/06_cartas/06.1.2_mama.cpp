#include <stdio.h>
#include <stdlib.h>
   
//DIRECCIÓN DE MEMORIA char *: son 4 BYTES
/*const char *carta = "🂡 "; DEFINICIÓN DE VARIABES QUE APUNTAN A LAS CONSTANTES, no deja cambiar la variable carta y se vuelve fija. DIRECCIÓN: F0*/
   
int main (int argc, char *argv[]) {
 char carta[] = "🂡 "; //PRINCIPIO DE LA CADENA .Variable LOCAL, MATRICES Y PUNTEROS SON LAS  MISMA COSA
 /* char *fin; //FINAL de la cadena.
  fin = carta + 3; // ES LO MISMO QUE PONER: char *fin = carta + 3; 
  ++(*fin);Se quita la variable carta porque con carta solo sabes como ir hasta el final pero no sabes como volver, y con la variable fin sabe como volver porque esta en la variable carta  *carta++;     suma 1 al F1, se lee lo que contiene, incremento lo que contiene carta, es lo contrario del & que lo que hace es lee lo que hay en la dirección.*/
 
 /*Notación de matrices
  * carta [3]++;
  *
  *
  * */
   
   
   
    ++(*(carta + 3));
 
 
    printf("%s\n", carta);
  
  
     return EXIT_SUCCESS;
}
