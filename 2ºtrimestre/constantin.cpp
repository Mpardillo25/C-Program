#include <stdio.h>

#define EURO 166.386

#define TECNICO 1
#define SOCIALES 2
#define CONTACTO 4

int main () {
  double input;

    printf("How much do you wanna change?");
    scanf("%lf", &input);//lee del tubo stdin caracteres mientras se cumpla un criterio: numeros R (reales), &direccion de memoria donde esta input.

    printf("%.2lf₧  => %.2lf€ \n", input, input /EURO);

    return 0;


}
