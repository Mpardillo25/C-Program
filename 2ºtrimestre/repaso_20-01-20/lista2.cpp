#include <stdio.h>
#include <stdlib.h>
  
/*Dear array de caracteres que se llame frase y preguntar al usuario una palabra y la leemos con scanf con ese array de caracteres*/
  int main (int argc, char *argv[]){
   
       char frase[10];
  
      printf ("Introduce una palabra: ");
      scanf ("%s", frase);//frase ya contiene una direccion de memoria y no hace falta poner &.

      printf ("La palabra introducida es: %s \n",frase);
  
      return EXIT_SUCCESS;
  }

