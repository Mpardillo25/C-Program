#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){

    int *p; //un puntero ocupa 4 bytes

    p = (int *) malloc (sizeof (int));
    
   *p = 2; //alli a donde apunta p voy a poner un 2
    
    printf ("%i\n", *p);
     
    free(p);
    return EXIT_SUCCESS;
}
