#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){

    int *p; //un puntero ocupa 4 bytes

    p = (int *) malloc (5 * sizeof (int)); //acabamos de crear un array ya que hemos reservado 5 espacios
    
   *p = 2; //alli a donde apunta p voy a poner un 2
   *(p+1) = 7;
   p[2] = 9;
    
    printf ("%i - %i -%i\n", *p, p[1], *(p+2));
     
    free(p);
    return EXIT_SUCCESS;
}
