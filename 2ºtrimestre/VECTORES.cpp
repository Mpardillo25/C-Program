#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main (){
    double buffer; //hace falta guardar las cordenadas
    double *vector = NULL; //lo creo en el HEAP y de momento solo es un puntero
    static int dimension = 0;
    char end;
    printf ("ej: (1.5 2 3.7).\tVector: ");
    scanf ("%*[(]"); //saca los parentesis de apertura

    do {
        vector = (double *) realloc(vector, (dimension+1) * sizeof(double)); //hago una reserva aumentando el espacio de la matriz hago una reserva de 8 bytes
        scanf("%lf",&buffer);
        vector[dimension++] = buffer;
    } while (!scanf(" %1[)]",&end));

    printf ("\n\t( ");
    for (int componente =0; componente<dimension; componente ++)
        printf ("%6.2lf" , vector[componente]);
    printf(" )\n");

    free(vector);
return EXIT_SUCCESS;
}
